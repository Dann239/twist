#pragma once

#include <twist/rt/sim/system/scheduler.hpp>

#include "params.hpp"

#include <twist/rt/sim/system/simulator.hpp>

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/intrusive/list.hpp>

namespace twist::rt::sim {

namespace system::scheduler::fair {

class Scheduler final : public IScheduler {
 public:
  using Params = fair::Params;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    bool IsEmpty() const override {
      return waiters_.IsEmpty();
    }

    void Push(Thread* waiter) override {
      return waiters_.PushBack(waiter);
    }

    Thread* Pop() override {
      return waiters_.PopFront();
    }

    Thread* PopAll() override {
      return Pop();
    }

    bool Remove(Thread* thread) override {
      if (waiters_.IsLinked(thread)) {
        waiters_.Unlink(thread);
        return true;
      } else {
        return false;
      }
    }

   private:
    wheels::IntrusiveList<Thread, SchedulerTag> waiters_;
  };

 public:
  Scheduler(Params params = Params())
      : params_(params),
        random_(params_.seed) {
  }

  bool NextSchedule() override {
    return false;
  }

  void Start(Simulator* simulator) override {
    simulator_ = simulator;

    WHEELS_VERIFY(run_queue_.IsEmpty(), "Inconsistent state");
  }

  // System

  void Interrupt(Thread* active) override {
    run_queue_.PushFront(active);
  }

  void SwitchTo(Thread* /*active*/, ThreadId /*next*/) override {
    WHEELS_PANIC("SwitchTo not supported by FairScheduler");
  }

  void Yield(Thread* active) override {
    Sched(active);
  }

  void Wake(Thread* waiter) override {
    Sched(waiter);
  }

  void Spawn(Thread* thread) override {
    Sched(thread);
  }

  void Exit(Thread*) override {
    // No-op
  }

  // Hints

  void LockFree(Thread* /*thread*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Thread* /*thread*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty();
  }

  Thread* PickNext() override {
    Thread* next = run_queue_.PopFront();

    size_t steps = ++next->sched.f1;

    if (!next->preemptive || (steps <= params_.time_slice)) {
      return next;
    } else {
      Sched(next);
      return PickNext();  // Try again
    }
  }

  void Remove(Thread* thread) override {
    if (run_queue_.IsLinked(thread)) {
      run_queue_.Unlink(thread);
    }
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>();
  }

  // Random

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  // Spurious

  bool SpuriousWakeup() override {
    return false;
  }

  bool SpuriousTryFailure() override {
    return false;
  }

 private:
  void Sched(Thread* thread) {
    thread->sched.f1 = 0;
    run_queue_.PushBack(thread);
  }

 private:
  const Params params_;
  Simulator* simulator_;
  twist::random::WyRand random_{42};
  wheels::IntrusiveList<Thread, SchedulerTag> run_queue_;
};

}  // namespace system::scheduler::fair

}  // namespace twist::rt::sim
