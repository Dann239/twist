#pragma once

#include <cstdint>
#include <vector>

namespace twist::rt::sim {

namespace system::scheduler::dfs {

enum ForkType : uint32_t {
  PickNext,
  WakeOne,
  Preempt,
  RandomChoice,
  SpuriousWakeup,
  SpuriousTryFailure,
};

struct Fork {
  ForkType type;
  size_t alts;
  size_t index;
};

using Schedule = std::vector<Fork>;

}  // namespace system::scheduler::dfs

}  // namespace twist::rt::sim
