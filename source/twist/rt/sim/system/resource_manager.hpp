#pragma once

namespace twist::rt::sim {

namespace system {

class ResourceManager {
 public:
  ResourceManager() {
    Warmup();
  }

  ~ResourceManager();

 private:
  void Warmup();

 private:
  //
};

ResourceManager* StaticResourceManager();

}  // namespace system

}  // namespace twist::rt::sim
