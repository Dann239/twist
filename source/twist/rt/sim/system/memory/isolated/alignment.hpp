#pragma once

#include <cstddef>
#include <cstdlib>
#include <cstdint>

namespace twist::rt::sim {

namespace system {

namespace memory::isolated {

//////////////////////////////////////////////////////////////////////

// Fundamental alignment
static inline const size_t kAlignment = 16;

static_assert(kAlignment >= alignof(std::max_align_t));

//////////////////////////////////////////////////////////////////////

inline bool IsAligned(char* ptr) {
  return (std::uintptr_t)ptr % kAlignment == 0;
}

inline char* Align(char* ptr) {
  while (!IsAligned(ptr)) {
    ++ptr;
  }
  return ptr;
}

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::sim
