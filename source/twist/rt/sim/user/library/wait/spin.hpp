#pragma once

#include <twist/rt/sim/user/scheduler/model.hpp>
#include <twist/rt/sim/user/syscall/yield.hpp>
#include <twist/rt/sim/user/syscall/abort.hpp>

#include <twist/build.hpp>

namespace twist::rt::sim {

namespace user {

class [[nodiscard]] SpinWait {
 public:
  SpinWait() = default;

  // Non-copyable
  SpinWait(const SpinWait&) = delete;
  SpinWait& operator=(const SpinWait&) = delete;

  // Non-movable
  SpinWait(SpinWait&&) = delete;
  SpinWait& operator=(SpinWait&&) = delete;

  void Spin() {
    Yield();
  }

  void operator()() {
    Spin();
  }

  [[nodiscard]] bool ConsiderParking() const;

  [[nodiscard]] bool KeepSpinning() const {
    return !ConsiderParking();
  }

 private:
  void Yield() {
    ++yield_count_;
    syscall::Yield();
  }

 private:
  size_t yield_count_ = 0;
};

inline void CpuRelax() {
  syscall::Yield();
}

}  // namespace user

}  // namespace twist::rt::sim
