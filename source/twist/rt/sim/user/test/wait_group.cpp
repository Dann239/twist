#include "wait_group.hpp"

#include <twist/rt/sim/user/syscall/futex.hpp>
#include <twist/rt/sim/user/scheduler/preemption_guard.hpp>

namespace twist::rt::sim {

namespace user::test {

void WaitGroup::Join(wheels::SourceLocation call_site) {
  if (count_ == 0) {
    return;
  }

  {
    // NB: Happens-before any wg thread
    left_.store(count_);
  }

  // Spawn

  UserStateNode* s = head_;
  while (s != nullptr) {
    syscall::Spawn(s);
    s = s->next;
  }

  do {
    system::WaiterContext waiter{system::FutexType::WaitGroup,
                                 "WaitGroup::Join", call_site};
    syscall::FutexWait(&done_, 0, &waiter);
  } while (done_.load() != 1);
}

void WaitGroup::AtFiberExit() noexcept {
  scheduler::PreemptionGuard guard;  // <- Merge with the last user action

  // NB: Join release sequences of wg threads
  if (left_.fetch_sub(1) == 1) {
    done_.store(1);

    system::WakerContext waker{"WaitGroup completion", ctor_source_loc_};
    syscall::FutexWake(&done_, 1, &waker);
  }
}

}  // namespace user::test

}  // namespace twist::rt::sim
