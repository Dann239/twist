#pragma once

namespace twist::rt::sim {

namespace user::syscall {

void Yield();

}  // namespace user::syscall

}  // namespace twist::rt::sim
