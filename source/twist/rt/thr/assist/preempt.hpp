#pragma once

namespace twist::rt::thr::assist {

inline void PreemptionPoint() {
  // No-op
}

}  // namespace twist::rt::thr::assist
