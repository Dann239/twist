#pragma once

namespace twist::rt::thr {

namespace assist {

// New

template <typename T, typename ... Args>
T* New(Args&& ... args) {
  return new T{std::forward<Args>(args)...};
}

// Memory access

void MemoryAccess(void* addr, size_t) {
  if (addr == nullptr) {
    wheels::Panic("Nullptr access");
  }
  // TODO: AddressSanitizer
}

// Ptr<T>

template <typename T>
struct Ptr {
  Ptr(T* p) {
    raw = p;
  }

  // operator T*

  operator T*() {
    return raw;
  }

  operator const T*() const {
    return raw;
  }

  // operator ->

  T* operator->() {
    return raw;
  }

  const T* operator->() const {
    return raw;
  }

  // operator *

  T& operator*() {
    return *raw;
  }

  const T& operator*() const {
    return *raw;
  }

  // operator &

  T** operator&() {
    return &raw;
  }

  T* const * operator&() const {
    return &raw;
  }

  // operator bool

  explicit operator bool() const {
    return raw != nullptr;
  }

  // raw pointer

  T* raw;
};

// ==

template <typename T>
bool operator==(Ptr<T> lhs, Ptr<T> rhs) {
  return lhs.raw == rhs.raw;
}

template <typename T>
bool operator==(Ptr<T> lhs, T* rhs) {
  return lhs.raw == rhs;
}

template <typename T>
bool operator==(T* lhs, Ptr<T> rhs) {
  return lhs == rhs.raw;
}

// !=

template <typename T>
bool operator!=(Ptr<T> lhs, Ptr<T> rhs) {
  return lhs.raw != rhs.raw;
}

template <typename T>
bool operator!=(Ptr<T> lhs, T* rhs) {
  return lhs.raw != rhs;
}

template <typename T>
bool operator!=(T* lhs, Ptr<T> rhs) {
  return lhs != rhs.raw;
}

// TODO: more operators

}  // namespace assist

}  // namespace twist::rt::thr
