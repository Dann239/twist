#pragma once

#include <twist/rt/thr/wait/futex/wake_key.hpp>

#include <atomic>
#include <chrono>

namespace twist::rt::thr {

namespace futex {

// Wait

void Wait(std::atomic<uint32_t>& atomic, uint32_t old,
          std::memory_order mo = std::memory_order::seq_cst);

bool WaitTimed(std::atomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout,
               std::memory_order mo = std::memory_order::seq_cst);

// Wake

WakeKey PrepareWake(std::atomic<uint32_t>& atomic);

void WakeOne(WakeKey key);
void WakeAll(WakeKey key);

}  // namespace futex

}  // namespace twist::rt::thr
