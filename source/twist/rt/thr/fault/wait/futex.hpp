#pragma once

#include <twist/rt/thr/fault/std_like/atomic.hpp>

#include <twist/rt/thr/wait/futex/wait.hpp>

#include <cstdint>

namespace twist::rt::thr::fault {

namespace futex {

void Wait(FaultyAtomic<uint32_t>& atomic, uint32_t old,
          std::memory_order mo = std::memory_order::seq_cst);

bool WaitTimed(FaultyAtomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout,
               std::memory_order mo = std::memory_order::seq_cst);

using rt::thr::futex::WakeKey;

WakeKey PrepareWake(FaultyAtomic<uint32_t>& atomic);

void WakeOne(WakeKey key);
void WakeAll(WakeKey key);

}  // namespace futex

}  // namespace twist::rt::thr::fault
