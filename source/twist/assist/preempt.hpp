#pragma once

/*
 * Preemption point for thread adversary / simulation scheduler
 *
 * void twist::assist::PreemptionPoint();
 */

#include <twist/rt/cap/assist/preempt.hpp>

namespace twist::assist {

using rt::cap::assist::PreemptionPoint;

}  // namespace twist::assist
