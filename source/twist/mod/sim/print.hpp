#pragma once

#include "simulator.hpp"
#include "sched/replay.hpp"

namespace twist::sim {

void Print(MainRoutine main, sched::Schedule schedule, SimulatorParams params = SimulatorParams{});

}  // namespace twist::sim
