#pragma once

#include "../simulator.hpp"

#include <twist/rt/sim/scheduler/fair/scheduler.hpp>

namespace twist::sim::sched {

using FairScheduler = twist::rt::sim::system::scheduler::fair::Scheduler;

}  // namespace twist::sim::sched
