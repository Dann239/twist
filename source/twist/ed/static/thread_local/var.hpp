#pragma once

#include <twist/rt/cap/static/thread_local/var.hpp>

// static thread_local T name
// → TWISTED_STATIC_THREAD_LOCAL_VAR(T, name)

// Usage: examples/thread_local/main.cpp
