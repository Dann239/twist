#include <wheels/test/framework.hpp>

#include <twist/cross.hpp>

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/std/thread.hpp>
#include <twist/ed/std/chrono.hpp>
#include <twist/ed/wait/futex.hpp>

using twist::ed::std::atomic;
using twist::ed::std::thread;

namespace this_thread = twist::ed::std::this_thread;

namespace futex = twist::ed::futex;

using namespace std::chrono_literals;

TEST_SUITE(Futex) {
  SIMPLE_TEST(Wait) {
    twist::cross::Run([] {
      atomic<uint32_t> flag{0};

      thread producer([&] {
        this_thread::sleep_for(1s);

        {
          auto wake_key = futex::PrepareWake(flag);
          flag.store(1);
          futex::WakeOne(wake_key);
        }
      });

      futex::Wait(flag, 0);
      ASSERT_EQ(flag.load(), 1);

      producer.join();
    });
  }

  SIMPLE_TEST(WakeKey) {
    twist::cross::Run([] {
      atomic<uint32_t> flag{0};
      auto key1 = futex::PrepareWake(flag);

      auto key2 = key1;  // Copyable
      WHEELS_UNUSED(key2);
    });
  }

  SIMPLE_TEST(DoNotWait) {
    twist::cross::Run([] {
      atomic<uint32_t> flag{1};

      futex::Wait(flag, 0);
      ASSERT_EQ(flag.load(), 1);
    });
  }

  SIMPLE_TEST(WaitTimed1) {
    twist::cross::Run([] {
      atomic<uint32_t> flag{0};

      thread producer([&] {
        this_thread::sleep_for(1s);

        {
          auto wake_key = futex::PrepareWake(flag);
          flag.store(1);
          futex::WakeOne(wake_key);
        }
      });

      futex::WaitTimed(flag, 0, 100ms);
      ASSERT_EQ(flag.load(), 0);

      producer.join();
    });
  }

  SIMPLE_TEST(WaitTimed2) {
    twist::cross::Run([] {
      atomic<uint32_t> flag{0};

      thread producer([&] {
        this_thread::sleep_for(1s);

        {
          auto wake_key = futex::PrepareWake(flag);
          flag.store(1);
          futex::WakeOne(wake_key);
        }
      });

      futex::WaitTimed(flag, 0, 10s);
      ASSERT_EQ(flag.load(), 1);

      producer.join();
    });
  }

  SIMPLE_TEST(DoNotWaitTimed) {
    twist::cross::Run([] {
      atomic<uint32_t> flag{1};

      futex::WaitTimed(flag, 0, 1s);
      ASSERT_EQ(flag.load(), 1);
    });
  }

  SIMPLE_TEST(MemoryOrder) {
    twist::cross::Run([] {
      atomic<uint32_t> flag{1};

      {
        // Compiles
        futex::Wait(flag, 0, std::memory_order::acquire);
      }

      {
        // Compiles
        futex::WaitTimed(flag, 0, 1s, std::memory_order::acquire);
      }
    });
  }
}
