#include <twist/sim.hpp>

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/std/mutex.hpp>
#include <twist/ed/std/shared_mutex.hpp>
#include <twist/ed/std/thread.hpp>

#include <twist/ed/c/abort.hpp>

#include <twist/assist/assert.hpp>
#include <twist/assist/memory.hpp>
#include <twist/assist/shared.hpp>

#include <wheels/core/compiler.hpp>

#include <fmt/core.h>

#include <cassert>
#include <stdexcept>

static_assert(twist::build::IsolatedSim());

using SimStatus = twist::sim::Status;

int main() {
  auto params = twist::sim::Simulator::Params{};
  params.crash_on_abort = false;

  {
    // twist::ed::c::abort()

    auto result = twist::sim::TestSim(params, [] {
      twist::ed::c::abort();

      WHEELS_UNREACHABLE();
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::UserAbort);
  }

  {
    // Unhandled exception in main thread

    auto result = twist::sim::TestSim(params, [] {
      throw std::runtime_error("Unhandled");

      WHEELS_UNREACHABLE();
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::UnhandledException);
    fmt::println("Stderr: {}", result.std_err);
  }

  {
    // Unhandled exception in user thread

    auto result = twist::sim::TestSim(params, [] {
      twist::ed::std::thread t([] {
        throw std::runtime_error("Unhandled");
      });

      t.join();

      WHEELS_UNREACHABLE();
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::UnhandledException);
  }

  {
    // twist::assist::Panic (Abort + stderr)

    auto result = twist::sim::TestSim(params, [] {
      twist::assist::Panic("Test");
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::UserAbort);
    fmt::println("Stderr: {}", result.std_err);
  }

#if !defined(NDEBUG)
  {
    // TWISTED_ASSERT

    auto result = twist::sim::TestSim(params, [] {
      TWIST_ASSERT(2 + 2 == 5, "Arithmetic failure");

      WHEELS_UNREACHABLE();
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::UserAbort);
    fmt::println("Stderr: {}", result.std_err);
  }

#endif

  {
    // TWISTED_VERIFY

    auto result = twist::sim::TestSim(params, [] {
      TWIST_ASSERT(2 + 2 == 5, "Arithmetic failure");

      WHEELS_UNREACHABLE();
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::UserAbort);
    fmt::println("Stderr: {}", result.std_err);
  }

  {
    // mutex

    {
      // unlock

      auto result = twist::sim::TestSim(params, [] {
      twist::ed::std::mutex mu;

      mu.unlock();

      WHEELS_UNREACHABLE();
    });

      assert(!result.Ok());
      assert(result.status == SimStatus::LibraryAssert);
      fmt::println("Stderr: {}", result.std_err);
    }

    {
      // mutex owner

      auto result = twist::sim::TestSim(params, [] {
        twist::ed::std::mutex mu;

        mu.lock();

        twist::ed::std::thread t([&] {
          mu.unlock();  // <- Aborted
        });

        t.join();

        WHEELS_UNREACHABLE();
      });

      assert(!result.Ok());
      assert(result.status == SimStatus::LibraryAssert);
      fmt::println("Stderr: {}", result.std_err);
    }
  }

  {
    // shared_mutex

    {
      // unlock

      auto result = twist::sim::TestSim(params, [] {
      twist::ed::std::shared_mutex mu;

      mu.unlock();

      WHEELS_UNREACHABLE();
    });

      assert(!result.Ok());
      assert(result.status == SimStatus::LibraryAssert);
      fmt::println("Stderr: {}", result.std_err);
    }

    {
      // unlock_shared

      auto result = twist::sim::TestSim(params, [] {
        twist::ed::std::shared_mutex mu;

        mu.unlock_shared();

        WHEELS_UNREACHABLE();
      });

      assert(!result.Ok());
      assert(result.status == SimStatus::LibraryAssert);
      fmt::println("Stderr: {}", result.std_err);
    }
  }

  {
    // thread

    {
      auto result = twist::sim::TestSim(params, [] {
      twist::ed::std::thread t([] {});
      // Aborted in thread dtor
    });

      assert(!result.Ok());
      fmt::println("Stderr: {}", result.std_err);
    }

    {
      auto result = twist::sim::TestSim(params, [] {
        twist::ed::std::thread t([] {});
        t.detach();
        t.join();  // <- Aborted

        WHEELS_UNREACHABLE();
      });

      assert(!result.Ok());
      assert(result.status == SimStatus::LibraryAssert);
      fmt::println("Stderr: {}", result.std_err);
    }
  }

  {
    // Memory

    {
      // Double-free

      auto result = twist::sim::TestSim(params, [] {
      int* p = new int{17};
      delete p;
      delete p;  // <- Aborted

      WHEELS_UNREACHABLE();
    });

      assert(!result.Ok());
      assert(result.status == SimStatus::MemoryDoubleFree);
      fmt::println("Stderr: {}", result.std_err);
    }

    {
      // Memory leak

      auto result = twist::sim::TestSim(params, [] {
        new int{1};
      });

      assert(!result.Ok());
      assert(result.status == SimStatus::MemoryLeak);
      fmt::println("Stderr: {}", result.std_err);
    }

    {
      // Heap-use-after-free

      auto result = twist::sim::TestSim(params, [] {
        struct Node {
          int data;
        };

        Node* n = new Node{7};
        [[maybe_unused]] int d1 = twist::assist::Ptr(n)->data;
        delete n;

        [[maybe_unused]] int d2 = twist::assist::Ptr(n)->data;

        WHEELS_UNREACHABLE();
      });

      assert(!result.Ok());
      assert(result.status == SimStatus::InvalidMemoryAccess);
      fmt::println("Stderr: {}", result.std_err);
    }
  }

  {
    // Deadlock 1

    auto result = twist::sim::TestSim(params, [] {
      twist::ed::std::mutex mu;

      mu.lock();
      mu.lock();
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::Deadlock);
    // fmt::println("Stderr: {}", result.std_err);
  }

  {
    // Deadlock 2

    auto result = twist::sim::TestSim(params, [] {
      while (true) {
        twist::ed::std::mutex mu;

        mu.lock();

        twist::ed::std::thread t([&] {
          mu.lock();  // <- Deadlocked
          mu.unlock();
        });

        t.join();  // <- Deadlocked
        mu.unlock();

        t.join();
      }
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::Deadlock);

    // fmt::println("Stderr: {}", result.std_err);
  }

  {
    // Data race

    twist::sim::sched::CoopScheduler scheduler{{42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> x{0};

      twist::ed::std::thread t([&] {
        x.Write(2);
      });

      x.Write(1);

      t.join();
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::DataRace);

    fmt::println("Stderr: {}", result.std_err);
  }

  {
    // Dead atomic access

    twist::sim::sched::CoopScheduler scheduler{{42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      auto* atom = new twist::ed::std::atomic_int64_t{7};

      delete atom;

      atom->store(1);
    });

    assert(!result.Ok());
    assert(result.status == SimStatus::InvalidMemoryAccess);

    fmt::println("Stderr: {}", result.std_err);
  }

  return 0;
}
