#include <twist/sim.hpp>
#include <twist/mod/sim/switch_to.hpp>

#include <twist/ed/std/thread.hpp>
#include <twist/ed/std/atomic.hpp>
#include <twist/ed/fmt/print.hpp>

#include <twist/assist/shared.hpp>

#include <fmt/core.h>

static_assert(twist::build::IsolatedSim());

int main() {
  {
    // Just data race

    twist::sim::sched::FairScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};

      twist::ed::std::thread t([&] {
        *data = 42;  // W
      });

      int d = data;  // R
      twist::ed::fmt::println("{}", d);

      t.join();
    });

    assert(!result.Ok());
    assert(result.status == twist::sim::Status::DataRace);
  }

  {
    // Message passing (thread::join)

    twist::sim::sched::FairScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};

      twist::ed::std::thread t([&] {
        *data = 42;  // W
      });

      t.join();

      int d = data;  // R
      twist::ed::fmt::println("{}", d);
    });

    assert(result.Ok());
  }

  {
    // Message passing (release / acquire)

    twist::sim::sched::FairScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};
      twist::ed::std::atomic<bool> flag{false};

      twist::ed::std::thread t([&] {
        *data = 42;  // W
        flag.store(true, std::memory_order::release);
      });

      while (!flag.load(std::memory_order::acquire)) {
        twist::ed::std::this_thread::yield();
      };

      int d = data;  // R
      twist::ed::fmt::println("{}", d);

      t.join();
    });

    assert(result.Ok());
  }

  {
    // Message passing (release / relaxed)

    twist::sim::sched::FairScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};
      twist::ed::std::atomic<bool> flag{false};

      twist::ed::std::thread t([&] {
        *data = 42;  // W
        flag.store(true, std::memory_order::release);
      });

      while (!flag.load(std::memory_order::relaxed)) {
        twist::ed::std::this_thread::yield();
      };

      int d = data;  // R
      twist::ed::fmt::println("{}", d);

      t.join();
    });

    assert(!result.Ok());
    assert(result.status == twist::sim::Status::DataRace);
  }

  {
    // Message passing (relaxed / acquire)

    twist::sim::sched::FairScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};
      twist::ed::std::atomic<bool> flag{false};

      twist::ed::std::thread t([&] {
        *data = 42;  // W
        flag.store(true, std::memory_order::relaxed);
      });

      while (!flag.load(std::memory_order::acquire)) {
        twist::ed::std::this_thread::yield();
      };

      int d = data;  // R
      twist::ed::fmt::println("{}", d);

      t.join();
    });

    assert(!result.Ok());
    assert(result.status == twist::sim::Status::DataRace);
  }

  {
    // Message passing (fences)

    twist::sim::sched::CoopScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};
      twist::ed::std::atomic<bool> flag{false};

      twist::ed::std::thread t2([&] {
        *data = 42;  // W
        twist::ed::std::atomic_thread_fence(std::memory_order::release);
        flag.store(true, std::memory_order::relaxed);
      });

      twist::sim::SwitchTo(2);

      bool f = flag.load(std::memory_order::relaxed);
      assert(f);
      twist::ed::std::atomic_thread_fence(std::memory_order::acquire);

      int d = data;  // R
      twist::ed::fmt::println("{}", d);

      t2.join();
    });

    assert(result.Ok());
  }

  {
    // shared_ptr / release sequence

    // T1: fa(refcount, rlx)
    // T2: fa(refcount, rlx)
    // T2: W
    // T2: fs(refcount, acq_rel)
    // T3: fa(refcount, rlx)
    // T1: fs(refcount, acq_rel)
    // T3: fs(refcount, acq_rel)
    // T3: R

    twist::sim::sched::CoopScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};

      twist::ed::std::atomic<int> refcount{1};  // 1

      twist::ed::std::thread t2([&] {
        int r1 = refcount.fetch_add(1, std::memory_order::relaxed);  // 2
        assert(r1 == 1);

        *data = 42;  // W

        int r2 = refcount.fetch_sub(1, std::memory_order::acq_rel);  // 3
        assert(r2 == 2);

        twist::sim::SwitchTo(3);
      });

      twist::ed::std::thread t3([&] {
        int r1 = refcount.fetch_add(1, std::memory_order::relaxed);  // 4
        assert(r1 == 1);

        twist::sim::SwitchTo(1);

        int r2 = refcount.fetch_sub(1, std::memory_order::acq_rel);  // 6
        assert(r2 == 1);  //  Last decrement

        int d = data;  // R
        twist::ed::fmt::print("{}", d);  // 42
      });

      twist::sim::SwitchTo(2);

      int r = refcount.fetch_sub(1, std::memory_order::acq_rel);  // 5
      assert(r == 2);

      twist::sim::SwitchTo(3);

      t2.join();
      t3.join();

      assert(refcount.load() == 0);
    });

    assert(result.Ok());
    assert(result.std_out == "42");
  }

  {
    // shared_ptr / release sequence, atomic_thread_fence

    // T1: fa(refcount, rlx)
    // T2: fa(refcount, rlx)
    // T2: W
    // T2: fs(refcount, rel)
    // T3: fa(refcount, rlx)
    // T1: fs(refcount, rel)
    // T3: fs(refcount, rel)
    // T3: atf(acq)
    // T3: R

    twist::sim::sched::CoopScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};

      twist::ed::std::atomic<int> refcount{1};  // 1

      twist::ed::std::thread t2([&] {
        int r1 = refcount.fetch_add(1, std::memory_order::relaxed);  // 2
        assert(r1 == 1);

        *data = 42;  // W

        int r2 = refcount.fetch_sub(1, std::memory_order::release);  // 3
        assert(r2 == 2);

        twist::sim::SwitchTo(3);
      });

      twist::ed::std::thread t3([&] {
        int r1 = refcount.fetch_add(1, std::memory_order::relaxed);  // 4
        assert(r1 == 1);

        twist::sim::SwitchTo(1);

        int r2 = refcount.fetch_sub(1, std::memory_order::release);  // 6
        assert(r2 == 1);  //  Last decrement
        twist::ed::std::atomic_thread_fence(std::memory_order::acquire);  // 7

        int d = data;  // R
        twist::ed::fmt::print("{}", d);  // 42
      });

      twist::sim::SwitchTo(2);

      int r = refcount.fetch_sub(1, std::memory_order::release);  // 5
      assert(r == 2);

      twist::sim::SwitchTo(3);

      t2.join();
      t3.join();

      assert(refcount.load() == 0);
    });

    assert(result.Ok());
    assert(result.std_out == "42");
  }

  {
    // shared_ptr / release sequence, missing atomic_thread_fence

    // T1: fa(refcount, rlx)
    // T2: fa(refcount, rlx)
    // T2: W
    // T2: fs(refcount, rel)
    // T3: fa(refcount, rlx)
    // T1: fs(refcount, rel)
    // T3: fs(refcount, rel)
    // T3: missing atf(acq)
    // T3: R

    twist::sim::sched::CoopScheduler scheduler{{.seed=42}};
    twist::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::assist::Shared<int> data{0};

      twist::ed::std::atomic<int> refcount{1};  // 1

      twist::ed::std::thread t2([&] {
        int r1 = refcount.fetch_add(1, std::memory_order::relaxed);  // 2
        assert(r1 == 1);

        *data = 42;  // W

        int r2 = refcount.fetch_sub(1, std::memory_order::release);  // 3
        assert(r2 == 2);

        twist::sim::SwitchTo(3);
      });

      twist::ed::std::thread t3([&] {
        int r1 = refcount.fetch_add(1, std::memory_order::relaxed);  // 4
        assert(r1 == 1);

        twist::sim::SwitchTo(1);

        int r2 = refcount.fetch_sub(1, std::memory_order::release);  // 6
        assert(r2 == 1);  //  Last decrement

        // twist::ed::std::atomic_thread_fence(std::memory_order::acquire);

        int d = data;  // R
        twist::ed::fmt::print("{}", d);  // 42
      });

      twist::sim::SwitchTo(2);

      int r = refcount.fetch_sub(1, std::memory_order::release);  // 5
      assert(r == 2);

      twist::sim::SwitchTo(3);

      t2.join();
      t3.join();

      assert(refcount.load() == 0);
    });

    assert(!result.Ok());
    assert(result.status == twist::sim::Status::DataRace);
  }

  return 0;
}
