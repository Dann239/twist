#include <twist/sim.hpp>

#include <twist/assist/memory.hpp>

#include <fmt/core.h>

#include <cassert>
#include <stdexcept>

static_assert(twist::build::IsolatedSim());

int main() {
  auto params = twist::sim::SimulatorParams{};
  params.crash_on_abort = false;

  {
    // twist::assist::MemoryAccess

    {
      auto result = twist::sim::TestSim(params, [&] {
      char* buf = new char[64];

      twist::assist::MemoryAccess(buf, 64);
      twist::assist::MemoryAccess(buf, 1);
      twist::assist::MemoryAccess(buf + 63, 1);
      twist::assist::MemoryAccess(buf + 32, 32);

      delete[] buf;
    });

      fmt::println("Stderr: {}", result.std_err);
      assert(result.Ok());
    }

    {
      auto result = twist::sim::TestSim(params, [&] {
        char* buf = new char[64];

        twist::assist::MemoryAccess(buf, 65);
      });

      assert(result.status == twist::sim::Status::InvalidMemoryAccess);
    }

    {
      auto result = twist::sim::TestSim(params, [&] {
        char* buf = new char[64];

        twist::assist::MemoryAccess(buf - 1, 3);
      });

      assert(result.status == twist::sim::Status::InvalidMemoryAccess);
    }
  }

  {
    // twist::assist::Ptr

    auto result = twist::sim::TestSim(params, [&] {
      struct Widget {
        void Foo(){};
      };

      twist::assist::Ptr<Widget> w = new Widget{};
      w->Foo();

      delete w.raw;
      w->Foo();
    });

    assert(result.status == twist::sim::Status::InvalidMemoryAccess);
  }

  return 0;
}
