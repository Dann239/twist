# Assist

Аннотации для проверок, подсказки для симулятора / планировщика.

Директория: [`twist/assist/`](/source/twist/assist)

## Инварианты

Заголовок: `twist/assist/assert.hpp`

```cpp
// Где-то в примитиве синхронизации
auto state = state_.exchange(0);
TWIST_ASSERT(state != 0);
```

## Память

Заголовок: `twist/assist/memory.hpp`

### `Ptr`

Обертка для указателя на динамически аллоцированный (т.е. с помощью `new`) объект,
которая проверяет адресуемость этого объекта при обращении к нему через `operator*` и
`operator->`.

```cpp
twist::assist::Ptr<Widget>* w = new Widget{42};

// Ptr<T> повторяет интерфейс указателя 

// explicit operator bool
if (w) {
  // operator-> выполняет проверку MemoryAccess(ptr, sizeof(T))
  fmt::println("{}", w->datum);
}

// operator* – MemoryAccess
*w;
```

Чтобы захватить source location обращения, нужно конструировать
`Ptr<T>` прямо в точке обращения:

```cpp
Widget* w = new Widget{42};

twist::assist::Ptr(w)->datum;  // Захватываем source location обращения
```

См. [демо с лок-фри стеком Трейбера](/demo/treiber_stack/treiber_stack.hpp).

### `MemoryAccess`

Явная инструментация обращения к диапазону динамической памяти, выделенной через
`new`.

```cpp
twist::assist::MemoryAccess(addr, size);
```

Проверка выполняется только в режиме симуляции с изоляцией памяти. В других режимах – no-op.

## Гонки

Заголовок: `twist/assist/shared.hpp`

Аннотация для разделяемой переменной, обращения к которой должны быть упорядочено
синхронизацией (мьютексами, атомиками, формально – через [happens-before](https://eel.is/c++draft/intro.races#def:happens_before))

```cpp
// Узел лок-фри очереди Michael-Scott
struct Node {
  twist::assist::Shared<T> datum;
  twist::ed::std::atomic<Node> next;
};
```

### API

```cpp
twist::assist::Shared<int> var{0};

twist::ed::std::thread t([&var] {
  *var = 1;  // operator* – запись
});

t.join();

int val = var;  // operator T – чтение
```

Вместо перегруженных операторов можно использовать методы `Write` и `Read`, которые захватывают контекст
в исходном коде для отображения в сообщении об ошибке в симуляции:

```cpp
var.Write(1);  // <- Захватываем source location записи 

int val = var.Read();  // Аналогично – для чтения
```

См. [демо с лок-фри стеком Трейбера](/demo/treiber_stack/treiber_stack.hpp).

## Случайность

Заголовок: `twist/assist/random.hpp`

### Choice

`DfsScheduler` перебирает все возможные решения о планировании потоков, но иногда
для полноты тестирования нужно перебирать еще и случайные числа, которые генерирует пользователь.

Практический пример – выбор случайной жертвы для воровства в work-stealing планировщике задач.

Стандартная библиотека использует `random_device` как генератор случайности,
но в точке обращения к нему невозможно понять, из какого диапазона 
пользователь хочет получить случайное число.

`twist::assist::Choice` – это API для генерации случайных чисел из небольшого диапазона.

### Пример

```cpp
twist::ed::std::random_device rd{};
twist::assist::Choice choice{rd};

// Диапазон значений – [0, alts)
size_t a = choice(/*alts=*/3);

// Диапазон значений – [lo, hi)
size_t b = choice(/*lo=*/1, /*hi=*/5);
```

### Потоки

Для сборки с потоками `Choice` использует переданный `random_device` для инициализации [вихря Мерсенна](https://en.cppreference.com/w/cpp/numeric/random/mersenne_twister_engine), который, в свою очередь,
использует для генерации случайных индексов.

## Pruning

Заголовочный файл: `twist/assist/prune.hpp`

Подсказка model checker-у, что текущее исполнение пошло по не-перспективному (для обнаружения ошибки)
пути, так что все поддерево расписаний с текущим префиксом может быть обрезано.

## Preemption

Заголовочный файл: `twist/assist/preempt.hpp`

_Twist_ внедряет сбои / перехватывает управление только в точках обращения к примитивам синхронизации.

С помощью вызова `PreemptionPoint()` можно запросить переключение в произвольной точке вашего кода.
